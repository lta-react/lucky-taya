import cup from '../../images/cup.png';
import award from "../../images/award.png";
import trifecta from "../../images/trifecta.png";
import "../../CSS/games-box-md-srn.css"
import 'bootstrap/dist/css/bootstrap.min.css';

const gamesBoxMdScrn = () => {


    return (
        <>


            <div className='container-fluid mt-5'>
            <div className='row'>

                <div className="col-md-6 live-streaming-container">
                <h1>HIII</h1>
                </div>

                <div className="col-md-5 d-none d-sm-block d-md-block bg-dark games-box-container">

                {/* PER GAME AND ALL PHOTO */}
                <div className="mt-2 d-flex justify-content-center align-items-center pergame-pick1-container1">

                    {/* per game photo container */}
                    <div className="per-game-photo1 col-md-10">
                    <img src={cup} height="60" />
                    <h1 className='perGame-txt'>PER GAME</h1>
                    <p className="text-center text-light">Tumaya sa kada rack habang nag lalaban</p>
                    </div>

                    {/* PICK ONE */}
                    <div className="pick1-photo1 col-md-10">
                    <img src={award} height="60" />
                    <h1 className='perGame-txt'>PICK-1</h1>
                    <p className="pick1-text text-center text-light">Tumaya sa mananalo bago magumpisa ang laban</p>
                    </div>

                </div>
                
                {/* NUMBER GAMES */}
                <div className="d-flex justify-content-center mb-5 mt-3">
                    
                    <div className='container'>
                    <div className='row d-flex justify-content-center'>

                        <div className="d-flex number-games-container col-10">

                        <img src={trifecta} height="70" className="mt-4" />

                        <div className="number-games-text">
                            <h1 className="number-games-header number-games-headline">NUMBER GAMES</h1>

                            <p className="text-light">Pumili ng 2 digit combination ng tatayaan sa 10 main Ring. <span className='ten-text'>10 pesos tama 4,000 pesos</span></p>

                        </div>

                        </div>

                    </div>

                    </div>

                </div>

                </div>
            </div>

            </div>



        
        </>
    )
}

export default gamesBoxMdScrn;